class CreateConnections < ActiveRecord::Migration[7.0]
  def change
    create_table :connections do |t|
      t.string :jira_site
      t.string :jira_email
      t.string :jira_api_token
      t.integer :telegram_chat_id

      t.timestamps
    end
  end
end
