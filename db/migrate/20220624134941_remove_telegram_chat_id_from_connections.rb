class RemoveTelegramChatIdFromConnections < ActiveRecord::Migration[7.0]
  def change
    remove_column :connections, :telegram_chat_id, :integer
  end
end
