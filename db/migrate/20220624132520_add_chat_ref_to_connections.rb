class AddChatRefToConnections < ActiveRecord::Migration[7.0]
  def change
    add_reference :connections, :chat, null: false, foreign_key: true
  end
end
