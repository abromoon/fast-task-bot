class ApplicationController < ActionController::Base
  before_action :set_locale

  def after_sign_in_path_for(*)
    user_path(current_user)
  end

  def after_sign_out_path_for(*)
    root_path
  end

  def set_locale
    I18n.locale = extract_locale
  end

  def extract_locale
    I18n.available_locales.include?(params[:locale]&.to_sym) ? params[:locale] : I18n.default_locale
  end

  def default_url_options
    { locale: I18n.locale }
  end
end
