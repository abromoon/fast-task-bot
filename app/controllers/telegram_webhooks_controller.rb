class TelegramWebhooksController < Telegram::Bot::UpdatesController
  def start!(*)
    if payload['chat']['id'].negative?
      start_message = 'Чтобы воспользоваться ботом, зарегистрируйтесь на сайте.'
      respond_with :message, text: start_message, parse_mode: 'Markdown'
    else
      respond_with :message, text: 'Для работы с ботом, добавьте его в групповой чат!'
    end
  end

  def help!(*)
    if payload['chat']['id'].negative?
      help_message = "Чтобы создать задачу на Jira, используя бота, отправьте следующее сообщение:\n\n" \
                     "`@#{Rails.application.credentials.telegram.bot.username || 'BotFather'} <название задачи>\n\n<описание задачи>`"
      respond_with :message, text: help_message, parse_mode: 'Markdown'
    else
      respond_with :message, text: 'Для работы с ботом, добавьте его в групповой чат!'
    end
  end

  def chat!
    if payload['chat']['id'].negative?
      respond_with :message, text: "Chat id: `#{payload['chat']['id']}`", parse_mode: 'Markdown'
    else
      respond_with :message, text: 'Для работы с ботом, добавьте его в групповой чат!'
    end
  end

  def my_chat_member(update)
    case update['new_chat_member']['status']
    when 'member'
      Chat.create!(id: update['chat']['id'], name: update['chat']['title'])
    when 'left'
      Chat.destroy(update['chat']['id'])
    end
  end

  def message(update)
    if update['chat']['id'].negative?
      rename_chat(update['chat']['id'], update['new_chat_title']) if update['new_chat_title']

      return if update['text'].nil?

      return unless update['text'].start_with?('@JoraJiraBot ')

      begin
        response = IssueCreator.call(update)
      rescue IssueCreator::IssueCreationError => e
        response = e.message
      rescue StandardError => e
        puts "#{e.message} (#{e.class})"
        response = "Не удалось создать задачу!\n\nНеизвестная ошибка."
      ensure
        reply_with :message, text: response, parse_mode: 'Markdown'
      end
    else
      reply_with :message, text: 'Для работы с ботом, добавьте его в групповой чат!'
    end
  end

  private

  def rename_chat(chat_id, new_name)
    chat = Chat.find(chat_id)
    chat.update(name: new_name)
  end
end
