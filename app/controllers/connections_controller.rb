class ConnectionsController < ApplicationController
  before_action :set_user
  before_action :set_connection, only: %i[show edit update destroy]
  before_action :set_chats, only: %i[new create edit update]

  # GET /connections/1 or /connections/1.json
  def show; end

  # GET /connections/new
  def new
    @connection = @user.connections.new
  end

  # GET /connections/1/edit
  def edit; end

  # POST /connections or /connections.json
  def create
    @connection = @user.connections.new(connection_params)

    respond_to do |format|
      if @connection.save
        Telegram.bot.send_message(chat_id: @connection.chat_id, text: "#{@connection.user.email} подключил этот чат к проекту #{@connection.project_key}!")
        format.html { redirect_to user_url(@user), notice: 'Connection was successfully created.' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /connections/1 or /connections/1.json
  def update
    respond_to do |format|
      if @connection.update(connection_params)
        format.html { redirect_to user_path(@user), notice: 'Connection was successfully updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    Telegram.bot.send_message(chat_id: @connection.chat_id, text: "#{@connection.user.email} отключил этот чат от проекта #{@connection.project_key}!")
    @connection.destroy
    respond_to do |format|
      format.html { redirect_to user_path(@user), notice: 'Connection was successfully destroyed.' }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_connection
    @connection = Connection.find(params[:id])
  end

  def set_chats
    @chats = Chat.left_joins(:connection).where(connection: { chat_id: [@connection&.chat_id, nil].uniq })
  end

  def set_user
    @user = User.find(params[:user_id])
  end

  def connection_params
    params.require(:connection).permit(:chat_id, :project_key, :title, :jira_site, :jira_email, :jira_api_token)
  end
end
