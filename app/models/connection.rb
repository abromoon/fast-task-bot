class Connection < ApplicationRecord
  class MyValidator < ActiveModel::Validator
    def validate(record)
      record.errors.add :name, 'Need a name starting with X please!' if record.jira_site.start_with? '1'
    end
  end

  belongs_to :user
  belongs_to :chat

  validates :jira_api_token, presence: true
  validates :jira_site, presence: true
  validates :jira_email, presence: true
  validates :chat_id, presence: true
  validates :project_key, presence: true

  validates :chat_id, uniqueness: true

  validates :jira_email, format: { with: URI::MailTo::EMAIL_REGEXP }

  validate :validate_connection

  def validate_connection
    options = {
      username:     jira_email,
      password:     jira_api_token,
      site:         jira_site,
      context_path: '',
      auth_type:    :basic
    }

    client = JIRA::Client.new(options)

    begin
      client.Project.find(project_key)
    rescue JIRA::HTTPError
      errors.add(:base, I18n.t('invalid_jira_connection'))
    rescue StandardError
      errors.add(:base, I18n.t('unknown_jira_connection_error'))
    end
  end
end
