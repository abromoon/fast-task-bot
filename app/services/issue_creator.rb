require 'pp'
class IssueCreator < ApplicationService
  class IssueCreationError < StandardError; end

  def initialize(message)
    @message = message
  end

  def call
    summary, description = message_parser(@message)

    raise IssueCreationError.new("Не удалось создать задачу!\n\nНеправильный формат задачи.") unless issue?(
      @message['text'], summary
    )

    begin
      @con = Connection.find_by!(chat_id: @message['chat']['id'])
    rescue ActiveRecord::RecordNotFound
      raise IssueCreationError.new("Не удалось создать задачу!\n\nЭта группа не связана с проектом.")
    rescue StandardError => e
      puts e.message
    else
      client = new_client(@con)
      issue = create_issue(client, summary, description, @con)
      issue_link = "#{client.request_client.options[:site]}browse/#{issue.key}"
      "Создана задача с номером [#{issue.key}](#{issue_link})"
    end
  end

  def message_parser(message)
    summary, description = message['text'].split(/\n\n/, 2)

    summary = summary.gsub('@JoraJiraBot ', '').squish
    description = "#{description}\n\n" \
                  "Эта задача была создана @#{message['from']['username']} в чате #{message['chat']['title']}\n" \
                  "#{Time.at(message['date']).strftime('%R %d.%m.%y')}"

    [summary, description]
  end

  def new_client(con)
    options = {
      username:     con.jira_email,
      password:     con.jira_api_token,
      site:         con.jira_site,
      context_path: '',
      auth_type:    :basic
    }

    JIRA::Client.new(options)
  end

  def issue?(text, summary)
    text.start_with?('@JoraJiraBot ') && !summary.empty?
  end

  def create_issue(client, summary, description, con)
    issue = client.Issue.build
    issue.save(
      { 'fields' =>
                    { 'summary'     => summary,
                      'description' => description,
                      'project'     =>
                                       {
                                         'key' => con.project_key
                                       },
                      'issuetype'   =>
                                       {
                                         'name' => 'Task'
                                       } } }
    )
    issue.fetch
    issue
  end
end
