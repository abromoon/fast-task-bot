Rails.application.routes.draw do
  scope '(/:locale)', locale: /en|ru/ do
    devise_for :users

    resources :users, only: :show do
      resources :connections, except: :index
    end
    
    root "home#index"
  end

  telegram_webhook TelegramWebhooksController
end